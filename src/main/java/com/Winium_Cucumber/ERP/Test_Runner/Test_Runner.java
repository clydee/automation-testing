package com.Winium_Cucumber.ERP.Test_Runner;


import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@RunWith(Cucumber.class)
@CucumberOptions(features="Features",
				 glue = "com.Winium_Cucumber.Automation_Test_Cases",
				 plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
				 dryRun = false,
				 monochrome = true,
				 tags = "@Run"
				 )

public class Test_Runner {
	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig("C:/Users/abhishek.kawathekar/EClipse/com.Winium_Cucumber.Automation"
				+ "/src//test/java/com/Winium_Cucumber/Automation_Test_Cases/extent-config1.xml");
		Reporter.setSystemInfo("IPv4Address", "192.168.45.171");


		Reporter.assignAuthor("Abhishek Kawathekar");
	}   
	}