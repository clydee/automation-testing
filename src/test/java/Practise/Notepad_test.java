package Practise;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.windows.WindowsDriver;

public class Notepad_test {
	
	 private static WindowsDriver NotepadSession = null;
		
	    @Test
	    public void setup() {
	        try {
	            DesiredCapabilities capabilities = new DesiredCapabilities();
	            capabilities.setCapability("app", "C:\\Windows\\System32\\notepad.exe");
	            NotepadSession = new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
	            NotepadSession.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	            NotepadSession.findElementByXPath("//*[@Name='Text Editor']").click();
	            NotepadSession.findElementByXPath("//*[@Name='Text Editor']").sendKeys("This my typing");
	            NotepadSession.getKeyboard().sendKeys(Keys.ENTER);
	            NotepadSession.findElementByXPath("//*[@Name='Text Editor']").sendKeys("This my second typing");
	            
	        }catch(Exception e){
	            e.printStackTrace();
	        } finally {
	        }
	    }

}
