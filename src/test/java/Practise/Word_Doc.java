package Practise;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.windows.WindowsDriver;

public class Word_Doc {

	 public static WindowsDriver driver = null;
	  

		

		@Test
		public void Word_Open() throws Exception{
			
			DesiredCapabilities appCapabilities = new DesiredCapabilities();
			appCapabilities.setCapability("app", "C:\\Program Files\\Microsoft Office\\Office15\\WINWORD.EXE");
			
			driver = new WindowsDriver(new URL ("http://127.0.0.1:4723"), appCapabilities);
			System.out.println("Application is launched");		
			Thread.sleep(3000);
		}
		
		
		@Test
		public void New_Open() throws Exception{
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_ALT); //holds down Alt key
				robot.keyPress(KeyEvent.VK_F); //Press F4
				robot.keyPress(KeyEvent.VK_L); //release Alt key
				Thread.sleep(3000);
				driver.findElementByName("Insert").click();
				driver.findElementByName("Pictures...").click();
		}
		
		}
