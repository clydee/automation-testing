
package Practise;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import io.appium.java_client.windows.WindowsDriver;
import io.appium.java_client.windows.WindowsElement;


public class Login_Test {
	
	public static WindowsDriver driver = null;
	
	
	
	
	@Test
	public void LoginERP() throws Exception{
		
		DesiredCapabilities appCapabilities = new DesiredCapabilities();
		appCapabilities.setCapability("app", "C:\\Users\\abhishek.kawathekar\\Desktop\\27-Sep-2018\\Release\\KI_INFACT.exe");
		
		driver = new WindowsDriver(new URL ("http://127.0.0.1:4723"), appCapabilities);
		System.out.println("Application is launched");		
		Thread.sleep(3000);
		
		
		driver.findElementByAccessibilityId("TextUserId").sendKeys("Dev");
		driver.findElementByAccessibilityId("TextPassword").sendKeys("mspark");
		driver.findElementByAccessibilityId("btn_Login").click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.name("MOB")).click();
		
		WebElement element = driver.findElement(By.name("MOBDropDown"));
		
		Actions action = new Actions(driver);
		
		action.moveToElement(element).perform();
		
		WebElement subElement = driver.findElement(By.name("Application Indent"));
		
		action.moveToElement(subElement);
		
		action.click();
		
		action.perform();
		
		
		driver.findElement(By.name("Insert")).click();
		
		String Window = driver.getWindowHandle();
		System.out.println("This is window handle "+Window);

		driver.findElementByAccessibilityId("Btn_SalesID").click();
		
		
		driver.switchTo().window(Window);
		
		System.out.println("switch done");
		
		driver.findElementByAccessibilityId("TextSearch").sendKeys("Abhishek");
		driver.findElementByAccessibilityId("btnSearch").click();
		Thread.sleep(2000);
		
		WebElement element1 = driver.findElement(By.name("Employee_Id Row 9"));
		
		Actions action1 = new Actions(driver);
		
		action1.moveToElement(element1);
		
		action1.doubleClick();
		
		action1.perform();
		
		driver.findElementByAccessibilityId("TextApplicantName").sendKeys("Test Automation 17");
		driver.findElementByAccessibilityId("TextCourierName").sendKeys("By Hand");
		driver.findElementByAccessibilityId("TextAWBNo").sendKeys("By Hand");
		
		
		driver.findElementByAccessibilityId("ComboOffCity").sendKeys("Mumbai");
		Robot robot = new Robot(); 
		robot.keyPress(KeyEvent.VK_DOWN);

		
		driver.findElementByAccessibilityId("ComboPayment").click();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		
		driver.findElementByAccessibilityId("TextTotalAmtRecd").sendKeys("20000");		
		driver.findElementByAccessibilityId("TextVoucherNo").sendKeys("1234aB%");
		driver.findElementByAccessibilityId("rdoChequeToHo").click();
		driver.findElementByAccessibilityId("dtChqRecAtHo").sendKeys("27/09/2018");
		driver.findElementByAccessibilityId("TextNoOfTerminals").sendKeys("1");
		driver.findElementByAccessibilityId("CheckECS_Opted").click();
		
		driver.findElementByAccessibilityId("ComboMearchantType").click();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		
		driver.findElementByAccessibilityId("ComboSourcedBy").sendKeys("Salesman");
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_S);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_S);
		
		Thread.sleep(2000);
		
		String Window1 = driver.getWindowHandle();
		System.out.println("This is window handle "+Window1);
		
		driver.switchTo().window(Window1);
		String message = driver.findElementByAccessibilityId("65535").getText();
		System.out.println("The message is "+message);
		
		int Start = message.length();
		int Startp = Start-12;
		
		String Appl_No = message.substring(Startp);
		
		System.out.println("The Appl_No is "+Appl_No);
		
		driver.findElementByAccessibilityId("2").click();
		
		}
		}

