package com.Winium_Cucumber.Automation_Test_Cases;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.cucumber.listener.Reporter;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.windows.WindowsDriver;
import junit.framework.Assert;

public class StepDefinition {

	public static WindowsDriver driver = null;
	public static String Window;
	public static String Appl_No;
	public static String timestamp;
	public static java.sql.Connection con;
	public static ResultSet rs;
	public static Statement smt;
	public static CallableStatement csmt;
	
	public static Map<String, List<Object>> map;
	public static Map<String, List<Object>> map1;
	
	@Before
	public void startReport() {
		File file = new File("C:\\Users\\abhishek.kawathekar\\EClipse\\com.Winium_Cucumber.Automation\\src\\test"
				+ "\\java\\com\\Winium_Cucumber\\Automation_Test_Cases\\DataFile.properties");
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();

		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("This is fun");
		
		Reporter.getExtentHtmlReport();
		Reporter.loadXMLConfig("C:/Users/abhishek.kawathekar/EClipse/com.Winium_Cucumber.Automation/src/test/java/com"
				+ "/Winium_Cucumber/Automation_Test_Cases/extent-config1.xml");
		
		Reporter.assignAuthor(prop.getProperty("Tester"));
	}
	
	
	@Given("^I Login to the ERP with Dev user\\.$")
	public void i_Login_to_the_ERP_with_Dev_user() throws Throwable {
		DesiredCapabilities appCapabilities = new DesiredCapabilities();
		appCapabilities.setCapability("app", "C:\\Users\\abhishek.kawathekar\\Desktop\\27-Sep-2018\\Release\\KI_INFACT.exe");
		
		driver = new WindowsDriver(new URL ("http://127.0.0.1:4723"), appCapabilities);
		System.out.println("Application is launched");		
		Thread.sleep(3000);
		Reporter.addStepLog("ERP is launched");
		System.out.println(System.class.toGenericString());
				
		driver.findElementByAccessibilityId("TextUserId").sendKeys("Dev");
		driver.findElementByAccessibilityId("TextPassword").sendKeys("mspark");
		driver.findElementByAccessibilityId("btn_Login").click();
		System.getProperty("java.classpath");
		
		Thread.sleep(5000);
		
		String Title = driver.getTitle();
		System.out.println(Title);
		
		Assert.assertTrue(Title.contains("MSWIPE TECHNOLOGIES PVT. LTD. (Dev UAT)"));
		
		Reporter.addStepLog("Login done Successfully");
	}
	
	@Then("^I find element by Name \"([^\"]*)\" and click on the same\\.$")
	public void i_find_element_by_Name_and_click_on_the_same(String arg1) throws Throwable {
		driver.findElement(By.name(arg1)).click();
		Reporter.addStepLog(arg1+" pressed Successfully");
		
	}
	
	@Then("^I find drop down with element name \"([^\"]*)\" and click on \"([^\"]*)\"\\.$")
	public void i_find_drop_down_with_element_name_and_click_on(String arg1, String arg2) throws Throwable {
		Thread.sleep(2000);
		
		WebElement element = driver.findElement(By.name("MOBDropDown"));
		
		Actions action = new Actions(driver);
		
		action.moveToElement(element).perform();
		
		WebElement subElement = driver.findElement(By.name("Application Indent"));
		
		action.moveToElement(subElement);
		
		action.click();
		
		action.perform();
		
		Reporter.addStepLog(arg2+" pressed Successfully");
	}
	
	@Then("^I find element by ID \"([^\"]*)\" and click on the same\\.$")
	public void i_find_element_by_ID_and_click_on_the_same(String arg1) throws Throwable {
		
		driver.findElementByAccessibilityId(arg1).click();
		Reporter.addStepLog(arg1+" pressed Successfully");
	}
	
	@Then("^I switch the window\\.$")
	public void i_switch_the_window() throws Throwable {
		
		driver.switchTo().window(Window);
		
	}
	
	@Then("^I find element by ID \"([^\"]*)\" and click on the same and I switch the window\\.$")
	public void i_find_element_by_ID_and_click_on_the_same_and_I_switch_the_window(String arg1) throws Throwable {
	    String Window = driver.getWindowHandle();
		System.out.println("This is window handle "+Window);

		driver.findElementByAccessibilityId(arg1).click();
		
		driver.switchTo().window(Window);
		
		System.out.println("switch done");
		}
	
	
	@Then("^I find element by ID \"([^\"]*)\" and write \"([^\"]*)\"\\.$")
	public void i_find_element_by_ID_and_write(String arg1, String arg2) throws Throwable {
		driver.findElementByAccessibilityId(arg1).sendKeys(arg2);
	}
	
	@Then("^I find elment by name \"([^\"]*)\" and double click on the same\\.$")
	public void i_find_elment_by_name_and_double_click_on_the_same(String arg1) throws Throwable {
		WebElement element1 = driver.findElement(By.name(arg1));
		
		Actions action1 = new Actions(driver);
		
		action1.moveToElement(element1);
		
		action1.doubleClick();
		
		action1.perform();
	}
	
	@Then("^I press down arrow key\\.$")
	public void i_press_down_arrow_key() throws Throwable {
		Robot robot = new Robot(); 
		robot.keyPress(KeyEvent.VK_DOWN);
	}
	
	@Then("^I press Enter key\\.$")
	public void i_press_Enter_key() throws Throwable {
		Robot robot = new Robot(); 
		robot.keyPress(KeyEvent.VK_ENTER);
	}
	
	@Then("^I press Alt key\\.$")
	public void i_press_Alt_key() throws Throwable {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
	}
	
	@Then("^I press S key\\.$")
	public void i_press_S_key() throws Throwable {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_S);
	}
	
	@Then("^I release Alt key\\.$")
	public void i_release_Alt_key() throws Throwable {
		Robot robot = new Robot();
		robot.keyRelease(KeyEvent.VK_ALT);
	}
	
	@Then("^I take the screen-shot of the \"([^\"]*)\"\\.$")
	public void i_take_the_screen_shot_of_the(String arg1) throws Throwable {
	    Thread.sleep(10000);
	    this.captureScreenshot(arg1);
	    String Filename = arg1+"_"+timestamp+".png";
	    
	    System.out.println(Filename);
	    Reporter.addScreenCaptureFromPath("C:\\Users\\abhishek.kawathekar\\Desktop\\Images1\\", Filename);
	    
	}
	
	
	@Then("^I verify the text on new Window \"([^\"]*)\" for element ID \"([^\"]*)\"\\.$")
	public void i_verify_the_text_on_new_Window_for_element_ID(String arg1, String arg2) throws Throwable {
	   
		Thread.sleep(1000);
		
		String Window1 = driver.getWindowHandle();
		System.out.println("This is window handle "+Window1);
		
		driver.switchTo().window(Window1);
		String message = driver.findElementByAccessibilityId("65535").getText();
		System.out.println("The message is "+message);
		
		Assert.assertTrue(message.contains(arg1));
		
		Reporter.addStepLog(message);
		
	}
	
	@Then("^I save the Appl_No from the message of element ID \"([^\"]*)\"\\.$")
	public void i_save_the_Appl_No_from_the_message_of_element_ID(String arg1) throws Throwable {
	   
		driver.findElementByAccessibilityId("65535").getText();
		
		String message = driver.findElementByAccessibilityId("65535").getText();
		
		int Start = message.length();
		int Startp = Start-12;
		
		Appl_No = message.substring(Startp);
		
		Reporter.addStepLog("The Appl_No is "+Appl_No);
		
		System.out.println("The Appl_No is "+Appl_No);
	}

	@Then("^I close the Application\\.$")
	public void i_close_the_Application() throws Throwable {
		Thread.sleep(1000);
	    driver.close();
	}
	
	@Given("^I login to the \"([^\"]*)\" and \"([^\"]*)\"\\.$")
	public void i_login_to_the_and(String arg1, String arg2) throws Throwable {
		File file = new File("C:\\Users\\abhishek.kawathekar\\EClipse\\com.Winium_Cucumber.Automation\\src\\test"
				+ "\\java\\com\\Winium_Cucumber\\Automation_Test_Cases\\DataFile.properties");
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();

		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Driver loaded");
		con = DriverManager.getConnection(
				"jdbc:jtds:sqlserver://" + prop.getProperty(arg1) + ":1433/" + prop.getProperty(arg2),
				prop.getProperty("UserName"), prop.getProperty("Password"));
		System.out.println("Database is connected");
		Reporter.addStepLog("Connection is establish on Server :" + prop.getProperty("DevServerName"));
		Reporter.addStepLog("Connection is establish on DataBaseName :" + prop.getProperty("DevDataBaseName"));
	}
	
	

	@When("^I query to the \"([^\"]*)\" table with parameter as \"([^\"]*)\" above Appl_No\\.$")
	public void i_query_to_the_table_with_parameter_as_above_Appl_No(String arg1, String arg2) throws Throwable {
		smt = con.createStatement();
		rs = smt.executeQuery("Select * from " + arg1 + " where " + arg2 + " = '" + Appl_No + "' ");
		Reporter.addStepLog("My Query is : " + "Select * from " + arg1 + " where " + arg2 + " = '" + Appl_No + "' ");
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		map = new HashMap<>(columnsNumber);
		for (int i = 1; i <= columnsNumber; ++i) {
			map.put(rsmd.getColumnName(i), new ArrayList<>());
		}
		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; ++i) {
				map.get(rsmd.getColumnName(i)).add(rs.getObject(i));
			}
		}
		System.out.println(map.toString());
	}
	
	@Then("^I am verifying the below results for above Appl_No\\.$")
	public void i_am_verifying_the_below_results_for_above_Appl_No(DataTable arg1) throws Throwable {
		List<List<String>> data = arg1.raw();
		for (int i = 0; i < data.get(0).size(); i++) {
			String Headers = data.get(0).get(i);
			String Values = data.get(1).get(i);
			System.out.println("For -->" + Headers);
			System.out.println(map.get(data.get(0).get(i)));
			String MapValues = map.get(data.get(0).get(i)).toString().replace("[", "").replace("]", "");
			System.out.println(MapValues);
			Reporter.addStepLog("Validating Values for " + Headers + " in table is : " + MapValues );
			Assert.assertEquals(Values, MapValues);
		}
		con.close();
	}
	
	

	public void captureScreenshot(String fileName)throws IOException
	{

	// Take the screenshot and store as file format
	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

	// Open the current date and time
	timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());

	//Copy the screenshot on the desire location with different name using current date and time
	FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" 
	+ fileName+"_"+timestamp+".png"));
	}
	
	
	
	}
