@Run
Feature: New Application indent with Cash Mode and ECS opted.

  Scenario: New Application indent with Cash Mode
    Given I Login to the ERP with Dev user.
    Then I find element by Name "MOB" and click on the same.
    And I find drop down with element name "MOBDropDown" and click on "Application Indent".
    Then I find element by Name "Insert" and click on the same.
    And I find element by ID "Btn_SalesID" and click on the same and I switch the window.
    And I find element by ID "TextSearch" and write "Abhishek".
    And I find element by ID "btnSearch" and click on the same.
    And I find elment by name "Employee_Id Row 9" and double click on the same.
    And I find element by ID "TextApplicantName" and write "Test Automation 63".
    And I find element by ID "TextCourierName" and write "By Hand".
    And I find element by ID "TextAWBNo" and write "By Hand".
    And I find element by ID "ComboOffCity" and write "Mumbai".
    And I press down arrow key.
    And I find element by ID "ComboPayment" and click on the same.
    And I press down arrow key.
    And I press Enter key.
    And I find element by ID "TextTotalAmtRecd" and write "20000".
    And I find element by ID "TextVoucherNo" and write "1234aB".
    And I find element by ID "rdoChequeToHo" and click on the same.
    And I find element by ID "dtChqRecAtHo" and write "27/09/2018".
    And I find element by ID "TextNoOfTerminals" and write "1".
    And I find element by ID "CheckECS_Opted" and click on the same.
    And I find element by ID "ComboMearchantType" and click on the same.
    And I press down arrow key.
    And I press Enter key.
    And I find element by ID "ComboSourcedBy" and write "Salesman".
    And I press down arrow key.
    And I press Enter key.
    And I press Alt key.
    And I press S key.
    And I press Enter key.
    And I release Alt key.
    And I take the screen-shot of the "Application_Indent".
    Then I verify the text on new Window "Application created successfully" for element ID "65535".
    And I save the Appl_No from the message of element ID "65535".
    And I find element by ID "2" and click on the same.
    And I close the Application.
    Given I login to the "DevServerName" and "DevDataBaseName".
    When I query to the "Application_Master" table with parameter as "Appl_No" above Appl_No.
    Then I am verifying the below results for above Appl_No.
      | RequestType | Appl_Name          | Prefix_Code | Source_By | Payment | AWBNo   | CourierName | DOR_Username | Voucher_No | DepositMode            | NoOfTermsReq | NoOfTerminals | MerchantType | Entry_SourcePoint |
      | New         | TEST AUTOMATION 63 | APPL        | Salesman  | Cash    | BY HAND | BY HAND     | DEV          | 1234aB     | Cheque/Cash sent to HO |         1.00 |             1 | Corporate    | ERP               |
